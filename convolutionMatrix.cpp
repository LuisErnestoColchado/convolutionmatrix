#include <iostream>
#include <vector>
#include <omp.h>
#include <stdio.h>

#define  MAXSIZE 2000

using namespace std;

int** kernel;
double timeomp;
int** matrixResult;
int** matrix;

void getMedia(int** pMatrix,int** pKernel, unsigned int t, unsigned int d, unsigned int l, unsigned int r, int &pMedia){
  unsigned int indexT, indexL,indexLaux;
  int sum = 0;

  if(d > 2) d = 2;
  if(r > 2) r = 2;
  if(t > 2) t =2;
  if(l > 2) l =2;
  indexT = 2 - t;
  indexL = 2 - l;
  if(indexT > 2) indexT = 0;
  if(indexL > 2) indexL = 0;
  int sizeRowResult = t+d+1;
  int sizeColResult = l+r+1;
  if(sizeRowResult > 5) sizeRowResult = 5;
  if(sizeColResult > 5) sizeColResult = 5;

  for(int i=0;i<sizeRowResult;i++)
  {
    indexLaux = indexL;

    for(int j=0;j<sizeColResult;j++)
    {
      sum += (matrix[t+i][l+j] * pKernel[indexT][indexLaux]);
      indexLaux++;
    }
    indexT++;
  }

  int num = (sizeRowResult*sizeColResult);

  pMedia = sum/num;
}

void comvolution(int** pMatrix,int** rMatrix, int** pKernel, int thread_count){
  unsigned int right, down;
  int size;
  int maxsize = MAXSIZE;
  size = MAXSIZE/(thread_count);
  int media;
  int iam;
  int index;
  int sizeThread;

  int i,j;
# pragma omp parallel num_threads(thread_count) private(iam, index, media, sizeThread, down, right, i, j) shared(size,maxsize,thread_count,pKernel)
  {
    iam = omp_get_thread_num();
    index = size * iam;
    sizeThread = index + size;

    for(i=index;i<sizeThread;i++)
    {
      for(j=0;j<maxsize;j++)
      {
        down = (maxsize-1) - i;
        right = (maxsize-1) - j;

        getMedia(matrix, pKernel, i, down, j, right, media);

        matrixResult[i][j] = media;
      }
    }
  }
}

int main(int argc, char** argv){

  int media;
  int thread_count = strtol(argv[1],  NULL, 10);

  kernel = new int *[5];
  for(int i=0; i < 5;i++){
    kernel[i] = new int[5];
  }

  for(int i=0;i<5;i++)
  {
    for(int j=0;j<5;j++)
    {
      kernel[i][j] = 1;
    }
  }

  matrixResult = new int *[MAXSIZE];
  for(int i=0; i < MAXSIZE;i++){
    matrixResult[i] = new int[MAXSIZE];
  }

  matrix = new int *[MAXSIZE];
  for(int i=0; i < MAXSIZE;i++){
    matrix[i] = new int[MAXSIZE];
  }


  for(int i=0;i<MAXSIZE;i++)
  {
    for(int j=0;j<MAXSIZE;j++)
    {
      matrix[i][j] = rand() % 5 + 1;
    }
  }

  timeomp = omp_get_wtime();
  comvolution(matrix,matrixResult, kernel, thread_count);
  timeomp = omp_get_wtime() - timeomp;
  cout << "el tiempo fue: " << timeomp << endl;

  return 0;
}
